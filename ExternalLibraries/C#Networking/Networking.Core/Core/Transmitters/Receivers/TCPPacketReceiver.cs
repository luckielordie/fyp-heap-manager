﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Core.Transmitters.Receivers
{
    public class TCPPacketReceiver
    {
        NetworkStream networkStream;
        BinaryReader streamReader;

        private bool connected = true;
        public bool Connected
        {
            get { return connected; }
            private set { connected = value;  }
        }

        public TCPPacketReceiver(Socket socket)
        {
            networkStream = new NetworkStream(socket);
            streamReader = new BinaryReader(networkStream);
        }

        public void ReceivePacket(ref ConcurrentQueue<string> receivePacketQueue)
        {
            int arrayLength;

            try
            {
                while ((arrayLength = streamReader.ReadInt32()) > 0)
                {
                    BinaryFormatter bFormatter = new BinaryFormatter();
                    MemoryStream memStream = new MemoryStream();

                    string receivePacket = null;
                    byte[] bytes = streamReader.ReadBytes(arrayLength);
                    memStream.Write(bytes, 0, bytes.Length);
                    memStream.Position = 0;
                    object deserialisedPacket = bFormatter.Deserialize(memStream);
                    receivePacket = deserialisedPacket as string;

                    if (receivePacket != null) receivePacketQueue.Enqueue(receivePacket);
                }
            }
            catch(Exception e)
            {
                Connected = false;
                Console.WriteLine("TCP Receive Error: {0}", e.Message);
                return;                
            }
        }
    }
}
