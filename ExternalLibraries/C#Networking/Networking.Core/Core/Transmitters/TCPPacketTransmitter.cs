﻿using Networking.Core.Transmitters.Receivers;
using Networking.Core.Transmitters.Senders;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Networking.Core.Transmitters
{
    public class TCPPacketTransmitter
    {
        private Thread receiveThread;
        private Thread callbackThread;

        private TCPPacketSender packetSender;
        private TCPPacketReceiver packetReciever;

        private Socket socket;

        private Action<string> receivedMessageCallback = null;
        private Action<string> connectionClosedCallback = null;
        private ConcurrentQueue<string> receivedPacketQueue = new ConcurrentQueue<string>();

        private bool connected = true;

        private string connectionID;

        public bool Connected
        {
            get { return connected; }
            private set { connected = value; }
        }

        public TCPPacketTransmitter(string connectionID, Socket socket)
        {
            this.connectionID = connectionID;
            this.socket = socket;
            packetReciever = new TCPPacketReceiver(socket);
            packetSender = new TCPPacketSender(socket);

            receiveThread = new Thread(new ThreadStart(ReceivePacketFunction));
            callbackThread = new Thread(new ThreadStart(CheckPacketQueue));

            receiveThread.Start();
            callbackThread.Start();
        }

        private void ReceivePacketFunction()
        {
            packetReciever.ReceivePacket(ref receivedPacketQueue);
        }

        public void SendPacket(Packet packet)
        {
            packetSender.SendPacket(packet);            
        }

        public void SendString(string stringToSend)
        {
            packetSender.SendString(stringToSend);
        }

        public void SetReceivedCallback(Action<string> callback)
        {
            receivedMessageCallback = callback;
        }

        public void SetClosedConnectionCallback(Action<string> callback)
        {
            connectionClosedCallback = callback;
        }

        public void CheckPacketQueue()
        {
            while (Connected)
            {
                string outPacket = null;
                if (receivedPacketQueue.Count > 0)
                {
                    if (receivedPacketQueue.TryDequeue(out outPacket)
                        && receivedMessageCallback != null)
                    {
                        receivedMessageCallback(outPacket);
                    }
                }

                //check connection still valid
                Connected = (packetReciever.Connected && packetSender.Connected);
            }

            if (connectionClosedCallback != null)
            {
                connectionClosedCallback(connectionID);
            }
        }
    }
}
