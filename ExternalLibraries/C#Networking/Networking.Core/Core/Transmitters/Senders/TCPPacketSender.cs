﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Networking.Core.Transmitters.Senders
{
    public class TCPPacketSender
    {
        NetworkStream networkStream;
        BinaryWriter streamWriter;

        private bool connected = true;
        public bool Connected
        {
            get { return connected; }
            private set { connected = value; }
        }

        public TCPPacketSender(Socket connectedSocket)
        {
            networkStream = new NetworkStream(connectedSocket);
            streamWriter = new BinaryWriter(networkStream);
        }

        private MemoryStream AddPacketToMemoryStream(Packet packet)
        {
            BinaryFormatter bFormatter = new BinaryFormatter();
            MemoryStream memStream = new MemoryStream();

            bFormatter.Serialize(memStream, packet);

            return memStream;
        }

        private MemoryStream AddPacketToMemoryStream(string packet)
        {
            BinaryFormatter bFormatter = new BinaryFormatter();
            MemoryStream memStream = new MemoryStream();

            bFormatter.Serialize(memStream, packet);

            return memStream;
        }

        public void SendPacket(Packet packet)
        {
            try
            {
                MemoryStream dataToSend = AddPacketToMemoryStream(packet);
                byte[] data = dataToSend.ToArray();
                streamWriter.Write(data.Length);
                streamWriter.Write(data);
                streamWriter.Flush();
            }catch(Exception e)
            {
                Connected = false;
                Console.WriteLine("Send Error: {0}", e.Message);
                return;             
            }
        }

        public void SendString(string stringToSend)
        {
            try
            {
                MemoryStream dataToSend = AddPacketToMemoryStream(stringToSend);
                byte[] data = dataToSend.ToArray();
                streamWriter.Write(data.Length);
                streamWriter.Write(data);
                streamWriter.Flush();
            }
            catch (Exception e)
            {
                Connected = false;
                Console.WriteLine("Send Error: {0}", e.Message);
                return;
            }
        }
    }
}
