﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Networking.Core.Connections
{
    public abstract class Connection
    {
        protected Socket socket;
        protected int portNumber;
        protected string ipAddress;
        protected bool connected = false;

        #region Properties
        public abstract bool Connected
        {
            get;
            protected set;
        }

        public int Port
        {
            get { return portNumber; }
            private set { portNumber = value; }
        }
        #endregion


        public Connection(string ipAddress, int port)
        {
            this.ipAddress = ipAddress;
            this.portNumber = port;
        }

        public Connection(Socket connectedSocket)
        {
            this.socket = connectedSocket;
        }

        protected bool ConnectToHost()
        {
            try
            {
                socket.Connect(ipAddress, portNumber);
            }catch(Exception e)
            {
                Console.WriteLine("Failed to Connect: {0}", e.Message);
                return false;
            }

            return true;
        }

        public abstract bool Connect();
        public abstract void SendString(string data);
        public abstract void SetReceivePacketCallback(Action<string> callback);
        public abstract void SetDisconnectedCallback(Action<string> callback);
    }
}
