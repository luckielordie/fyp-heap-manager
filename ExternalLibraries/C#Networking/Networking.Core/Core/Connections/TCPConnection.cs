﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Networking.Core;
using System.IO;
using Networking.Core.Transmitters;

namespace Networking.Core.Connections
{
    public class TCPConnection : Connection
    {
        private TCPPacketTransmitter transmitter;

        public override bool Connected
        {
            get
            {
                return transmitter.Connected;
            }
            protected set
            {
                //Connected = transmitter.Connected;
            }
        }

        private string connectionID = "";
      
        public TCPConnection(string connectionID, string ipAddress, int port) : base(ipAddress, port)
        {
            this.connectionID = connectionID;
            socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
        }

        public TCPConnection(string connectionID, Socket connectedSocket) : base(connectedSocket)
        {
            this.transmitter = new TCPPacketTransmitter(connectionID, socket);
        }

        public override bool Connect()
        {
            if(ConnectToHost())
            {
                this.transmitter = new TCPPacketTransmitter(connectionID, socket);
                Connected = true;
                return Connected;
            }

            return Connected;
        }

        public override void SendPacket(Packet data)
        {
            transmitter.SendPacket(data);
        }

        public override void SendString(string data)
        {
            transmitter.SendString(data);
        }

        public override void SetReceivePacketCallback(Action<string> callback)
        {
            this.transmitter.SetReceivedCallback(callback);
        }

        public override void SetDisconnectedCallback(Action<string> callback)
        {
            this.transmitter.SetClosedConnectionCallback(callback);
        }
    }
}
