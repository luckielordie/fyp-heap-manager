﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;

namespace Networking.Core.Connections
{
    public class UDPConnection : Connection
    {
        private Thread receiveThread;
        private Action<Packet> receiveCallback = null;
        private Action<string> disconnectedCallback = null;

        private UdpClient udpListener;
        private IPEndPoint endPoint;
        private string connectionID;

        public override bool Connected
        {
            get
            {
                return true;
            }
            protected set
            {
                //Connected = transmitter.Connected;
            }
        }

        public UDPConnection(string connectionID, string ipAddress, int listenPort, int sendPort)
            : base(ipAddress, listenPort)
        {
            this.connectionID = connectionID;
            udpListener = new UdpClient(listenPort);
            endPoint = new IPEndPoint(IPAddress.Parse(ipAddress), sendPort);

            socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            
            receiveThread = new Thread(new ThreadStart(CheckReceive));
            receiveThread.Start();
        }

        public override bool Connect()
        {
            return true;    
        }

        public override void SendPacket(Packet data)
        {
            BinaryFormatter bFormatter = new BinaryFormatter();
            MemoryStream memStream = new MemoryStream();
            bFormatter.Serialize(memStream, data);

            socket.SendTo(memStream.ToArray(), endPoint);
        }

        public override void SetDisconnectedCallback(Action<string> callback)
        {
            disconnectedCallback = callback;
        }

        private void CheckReceive()
        {
            try
            {
                while (true)
                {
                    BinaryFormatter bFormatter = new BinaryFormatter();
                    MemoryStream memStream = new MemoryStream();

                    Packet receivePacket = null;
                    byte[] bytesReceived = udpListener.Receive(ref endPoint);

                    memStream.Write(bytesReceived, 0, bytesReceived.Length);
                    memStream.Position = 0;
                    object deserialisedPacket = bFormatter.Deserialize(memStream);
                    receivePacket = deserialisedPacket as Packet;

                    if (receivePacket != null) receiveCallback(receivePacket);
                }

            }catch(Exception e)
            {
                udpListener.Close();
                Console.WriteLine(e.Message);
                return;
            }

            if (disconnectedCallback != null) disconnectedCallback(connectionID);
        }
    }
}
