﻿using Networking.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Networking.Core.Connections;

namespace Networking.Server
{
    public class ServerClient : Client.Client
    {
        private Guid clientID;
        private Server server;
        private static int receiveUDP = 5555;
        private static int sendUDP = 6666;

        private string tcpConnectionID = "clientTCP";
        private string udpConnectionID = "clientUDP";

        public Guid ID
        {
            get { return this.clientID; }
        }

        public ServerClient(Socket clientSocket, Server server)
        {
            this.server = server;
            this.clientID = Guid.NewGuid();

            //setup new tcp connection
            this.connectionDictionary.Add(tcpConnectionID, new TCPConnection(tcpConnectionID, clientSocket));
            this.connectionDictionary[tcpConnectionID].SetReceivePacketCallback(this.OnRecievePacket);
            this.connectionDictionary[tcpConnectionID].SetDisconnectedCallback(this.OnDisconnect);
            this.OnConnect(tcpConnectionID);            

            receiveUDP++;
            sendUDP++;
        }

        protected override void OnConnect(string connectionID)
        {
            Console.WriteLine("CLIENT[{0}] : CREATED_CONNECTION: {1}", clientID, connectionID);
        }

        protected override void OnDisconnect(string connectionID)
        {
            Console.WriteLine("CLIENT[{0}] : CLOSED_CONNECTION: {1}", clientID, connectionID);
        }

        protected override void OnRecieveString(string packet)
        {
            server.ReceivePacket(packet);            
        }

        public void CreateUDPConnection()
        {
            CreateConnection(new ConnectionConfig(udpConnectionID, ConnectionProtocol.UDP, "127.0.0.1", sendUDP, receiveUDP));

            //create UDP Connection for connected clients
            ConnectionConfig config = new ConnectionConfig("serverUDP", ConnectionProtocol.UDP, "127.0.0.1", receiveUDP, sendUDP);
            SendPacket(tcpConnectionID, new Packet("OpenUDP", config));
        }


    }
}
