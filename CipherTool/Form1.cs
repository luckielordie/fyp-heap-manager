﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CipherTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Load += new EventHandler(this.Form1_Load);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadXMLData();
        }

        private void LoadXMLData()
        {
            SnapshotTreeController.UpdateTreeControl(snapshotTreeView);
        }

        private void tabControl_Click(object sender, EventArgs e)
        {

        }

        private void propertyGrid1_Click(object sender, EventArgs e)
        {

        }

        private void snapshotTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            typeTreeView.Nodes.Clear();
            propertyGrid1.SelectedObject = e.Node.Tag;

            if(e.Node.Tag is Snapshot)
            {
                Snapshot snapshot = e.Node.Tag as Snapshot;
                foreach(TypeDescription typeDesc in snapshot.SnapshotTypes)
                {
                    typeTreeView.Nodes.Add(new TreeNode("[TAGNAME: " + typeDesc.Name + " SIZE: " + typeDesc.Size / 1000 + "KB" + "]"){ Tag = typeDesc });
                }
            }else if(e.Node.Tag is Allocation)
            {
                Allocation alloc = e.Node.Tag as Allocation;
                TypeDescription typeDesc = alloc.Parent.GetTypeDescription(alloc.Type);
                typeTreeView.Nodes.Add(new TreeNode("[TAGNAME: " + typeDesc.Name + " SIZE: " + alloc.Size + "B" + "]") { Tag = typeDesc });
            }
        }

        private void compareSnapshotButton_Click(object sender, EventArgs e)
        {
            List<Snapshot> selectedSnapshots = SnapshotTreeController.GetSelectedSnapshots();

            if (selectedSnapshots[0] == null || selectedSnapshots[1] == null) return;

            List<Allocation> allocationDiffs = Snapshot.GetDiffAllocations(selectedSnapshots[0], selectedSnapshots[1]);

            SnapshotTreeController.UpdateTree(allocationDiffs, snapshotTreeView);
        }

        private void snapshotButton_Click(object sender, EventArgs e)
        {
            
        }
    }
}
