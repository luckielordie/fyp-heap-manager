﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CipherTool
{
    public class Allocation
    {

        #region PROPERTIES
        public Snapshot Parent
        {
            get;
            private set;
        }

        public string Type
        {
            get;
            private set;
        }

        public int Size
        {
            get;
            private set;
        }

        public int Alignment
        {
            get;
            private set;
        }

        public string File
        {
            get;
            private set;
        }

        public int Line
        {
            get;
            private set;
        }

        public string Function
        {
            get;
            private set;
        }
#endregion

        public Allocation(string allocationType, int allocationSize, int allocationAlignment, string fileName, int lineNumber, string functionName, Snapshot parent)
        {
            Parent = parent;
            Type = allocationType;
            Size = allocationSize;
            Alignment = allocationAlignment;
            File = fileName;
            Line = lineNumber;
            Function = functionName;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Allocation)
            {
                Allocation allocCompare = obj as Allocation;
                //make sure all properties match
                if (Type != allocCompare.Type) return false;
                if (Size != allocCompare.Size) return false;
                if (Alignment != allocCompare.Alignment) return false;
                if (File != allocCompare.File) return false;
                if (Line != allocCompare.Line) return false;
                if (Function != allocCompare.Function) return false;

                return true;
            }

            return false;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ALLOCATION [");
                sb.Append(" TYPE: " + Type);
                sb.Append(" SIZE: " + Size);
                sb.Append(" ALIGN: " + Alignment);
                sb.Append(" FILE: " + File);
                sb.Append(" LINE: " + Line);
                sb.Append(" FUNCTION: " + Function);
            sb.Append(" ]");

            return sb.ToString();
        }
    }
}
