﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CipherTool
{
   public class XMLSnapshotParser
    {

       private static Snapshot CreateSnapshot(XmlNode snapshotNode)
       {
           Snapshot snapshot = new Snapshot();
           List<Allocation> allocs = CreateAllocations(snapshotNode, snapshot);
           snapshot.SetAllocs(allocs);
           return snapshot;
       }

       private static List<Allocation> CreateAllocations(XmlNode snapshotNode, Snapshot parent)
       {
           List<Allocation> allocations = new List<Allocation>();

           foreach(XmlNode child in snapshotNode.ChildNodes)
           {
               allocations.Add(new Allocation(
                   child.Attributes["allocationType"].Value,
                   Int32.Parse(child.Attributes["allocationSize"].Value),
                   Int32.Parse(child.Attributes["allocationAlignment"].Value),
                   child.Attributes["fileName"].Value,
                   Int32.Parse(child.Attributes["lineNumber"].Value),
                   child.Attributes["functionName"].Value,
                   parent));
           }

           return allocations;
       }

       public static List<Snapshot> ParseFile(string filePath)
       {
           List<Snapshot> snapshotList = new List<Snapshot>();

           XmlDocument doc = new XmlDocument();
           try
           {
               doc.Load(filePath);
           }
           catch(Exception e)
           {
               
               return new List<Snapshot>();
           }
           XmlNodeList nodes = doc.GetElementsByTagName("Root");

           foreach (XmlNode snapshot in nodes[0].ChildNodes)
           {
               snapshotList.Add(CreateSnapshot(snapshot));
           }

           return snapshotList;
       }
    }
}
