#ifndef STACKALLOCATORPROFILER_H
#define STACKALLOCATORPROFILER_H

#include <IAllocatorProfiler.h>
#include "CipherManager.h"

class StackAllocatorProfiler : public IAllocatorProfiler
{
public:
	static long double AllocateAndDeallocateTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& stackAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::StackAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nSTACK ALLOCATOR ALLOC AND DEALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(stackAllocator, "SA_TEST", DataType);
			}

			//free all allocated memory
			for (int j = numAllocations - 1; j >= 0; j--)
			{
				Cipher::CIPHER_FREE(stackAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			
			//end probe here
			pSamples[i] = stopwatch.Stop();
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &stackAllocator);
		return IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
	}

	static long double AllocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& stackAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::StackAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nSTACK ALLOCATOR ALLOC ONLY START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(stackAllocator, "SA_TEST", DataType);
			}
			pSamples[i] = stopwatch.Stop();

			//free all allocated memory
			for (int j = numAllocations - 1; j >= 0; j--)
			{
				Cipher::CIPHER_FREE(stackAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &stackAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double DeallocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& stackAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::StackAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nSTACK ALLOCATOR DEALLOC ONLY START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];

			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(stackAllocator, "SA_TEST", DataType);
			}

			stopwatch.Start();
			//free all allocated memory
			for (int j = numAllocations - 1; j >= 0; j--)
			{
				Cipher::CIPHER_FREE(stackAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			
			//end probe here
			pSamples[i] = stopwatch.Stop();
			delete[] ptr;
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &stackAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}
};

#endif