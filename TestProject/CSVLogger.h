#ifndef CSVLOGGER_H
#define CSVLOGGER_H

#include <string>
#include <fstream>

class CSVLogger
{
private:
	std::string m_fileName;
public:
	CSVLogger(std::string fileName)
	{
		m_fileName = fileName;
		std::ofstream ofs;
		ofs.open(m_fileName, std::ofstream::trunc);
		ofs.close();
	}

	void AddValue(std::string string)
	{
		std::ofstream ofs;
		ofs.open(m_fileName, std::ofstream::app);
		ofs << string;
		ofs << ",";
		ofs.close();
	}

	void AddValue(long double number)
	{
		std::ofstream ofs;
		ofs.open(m_fileName, std::ofstream::app);
		ofs << number;
		ofs << ",";
		ofs.close();
	}

	void AddNewLine()
	{
		std::ofstream ofs;
		ofs.open(m_fileName, std::ofstream::app);
		ofs << "\n";
		ofs.close();
	}
};

#endif