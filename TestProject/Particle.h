#ifndef PARTICLE_H
#define PARTICLE_H

#include <random>

class Particle
{
private:
	int m_lifeSpan;
	int m_startTime;
	int m_elapsedTime;

public:
	Particle()
	{
		m_startTime = 0;
		m_lifeSpan = rand() % 1000 + 500;
		m_elapsedTime = 0;
	}

	~Particle()
	{
	}

	void Update(int elapsedMilliseconds)
	{
		m_elapsedTime += elapsedMilliseconds;
	}

	bool isDead()
	{
		return (m_startTime + m_lifeSpan) <= m_elapsedTime;
	}
};

#endif