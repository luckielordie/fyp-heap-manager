#ifndef NEWKEYWORD_H
#define NEWKEYWORD_H

#include "IAllocatorProfiler.h"

static class NewKeywordProfiler : public IAllocatorProfiler
{
public:
	static long double AllocateAndDeallocateTime(Stopwatch& stopwatch, int numAllocations)
	{
		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nNEW KEYWORD ALLOC AND DEALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = new DataType();
			}

			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				delete ptr[j];
				ptr[j] = nullptr;
			}
			pSamples[i] = stopwatch.Stop();
			delete[] ptr;
			ptr = nullptr;
		}
		long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double AllocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nNEW KEYWORD ALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = new DataType();
			}
			pSamples[i] = stopwatch.Stop();

			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				delete ptr[j];
				ptr[j] = nullptr;
			}
			delete[] ptr;
			ptr = nullptr;
		}

		long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double DeallocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nNEW KEYWORD DEALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			DataType** ptr = new DataType*[numAllocations];
			
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = new DataType();
			}			

			//free all allocated memory
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				delete ptr[j];
				ptr[j] = nullptr;
			}
			pSamples[i] = stopwatch.Stop();
			delete[] ptr;
			ptr = nullptr;
		}

		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}
};

#endif