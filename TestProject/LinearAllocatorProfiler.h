#ifndef LINEARALLOCATORPROFILE_H
#define LINEARALLOCATORPROFILE_H

#include "IAllocatorProfiler.h"
#include "CipherManager.h"

class LinearAllocatorProfiler : IAllocatorProfiler
{
public:
	static long double AllocateAndDeallocateTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& linearAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::LinearAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nLINEAR ALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(linearAllocator, "LA_TEST", DataType);
			}

			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				Cipher::CIPHER_FREE(linearAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			
			//end probe here
			pSamples[i] = stopwatch.Stop();
			delete[] ptr;
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &linearAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double AllocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& linearAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::LinearAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nLINEAR ALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(linearAllocator, "LA_TEST", DataType);
			}
			pSamples[i] = stopwatch.Stop();

			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				Cipher::CIPHER_FREE(linearAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			delete[]ptr;
			
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &linearAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double DeallocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& linearAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::LinearAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nLINEAR ALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(linearAllocator, "LA_TEST", DataType);
			}

			stopwatch.Start();
			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				Cipher::CIPHER_FREE(linearAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			
			//end probe here
			pSamples[i] = stopwatch.Stop();
			delete[]ptr;
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &linearAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}
};

#endif