#define _WINSOCKAPI_ //prevent re-inclusion of winsock

#include <iostream>
#include <limits>
#include "TestDefines.h"

#define CIPHER_LOGGING_ENABLED 0
#define CIPHER_ONLY_HEAP 0
#include <CipherManager.h>
#include <Stopwatch.h>
#include <NewKeywordProfiler.h>
#include <LinearAllocatorProfiler.h>
#include <StackAllocatorProfiler.h>
#include <PoolAllocatorProfiler.h>

#include <CSVLogger.h>
#include <random>
#include <chrono>
#include <Particle.h>

#include <Timer.h>
#include <vector>


using namespace std;
using namespace Cipher;
using namespace Cipher::Allocators;

void PerformLinearAllocatorShowcase(Stopwatch& stopwatch, CSVLogger& logger, int numAllocations)
{
	printf("LINEAR ALLOCATOR SHOWCASE [NUM_SAMPLES: %i, NUM ALLOCS PER SAMPLE: %i]\n", NUM_SAMPLE, NUM_ALLOCS);	
	long double linearAllocatorAverage = -1.0f;
	long double newKeywordAverage = -1.0f;

	linearAllocatorAverage = LinearAllocatorProfiler::AllocateAndDeallocateTime(stopwatch, numAllocations);
	newKeywordAverage = NewKeywordProfiler::AllocateAndDeallocateTime(stopwatch, numAllocations);

	//profile stuff here
	printf("\nNEW KEYWORD AVERAGE ALLOC TIME:\t\t%fms\n", newKeywordAverage);
	printf("LINEAR ALLOCATOR AVERAGE ALLOC TIME:\t%fms\n", linearAllocatorAverage);

	logger.AddValue("NUMBER OF ALLOCATIONS");
	logger.AddValue(numAllocations);
	logger.AddValue("NUMBER OF SAMPLES");
	logger.AddValue(NUM_SAMPLE);
	logger.AddNewLine();
	logger.AddValue("NEW ALLOC");
	logger.AddValue(newKeywordAverage);
	logger.AddNewLine();
	logger.AddValue("LINEAR ALLOC");
	logger.AddValue(linearAllocatorAverage);
	logger.AddNewLine();
	logger.AddNewLine();
	
}

void PerformPoolAllocatorShowcase(Stopwatch& stopwatch, CSVLogger& logger, int numParticles)
{
	printf("\nPOOL ALLOCATOR SHOWCASE [NUM PARTICLES: %i]\n", numParticles);
	int seed = 133654047;
	srand(seed); //seed random number generation for test

	int* pPoolSamples = new int[NUM_SAMPLE];
	int* pNewSamples = new int[NUM_SAMPLE];
	for (int j = 0; j < NUM_SAMPLE; j++)
	{
		printf("SAMPLE NUM: %i", j);
		//setup Pool Allocator
		printf("SETTING UP POOL ALLOCATOR TEST...\n");
		Allocator* poolAllocator = CIPHER.CreateAllocator<PoolAllocator>(CIPHER.GlobalAllocator(), numParticles * sizeof(Particle), sizeof(Particle), __alignof(Particle));
		vector<Particle*>pParticles = vector<Particle*>(numParticles);
		srand(seed);
		for (int i = 0; i < numParticles; i++)
		{
			pParticles[i] = CIPHER_NEW(*poolAllocator, "PARTICLE_TEST", Particle);
		}

		int poolNewParticleCount = 0;
		clock_t frameStart = clock();
		int elapsedTime = 0;
		stopwatch.Start();
		while (poolNewParticleCount <= numParticles)
		{
			for (int i = 0; i < numParticles; i++)
			{
				Particle* pParticle = pParticles.at(i);

				if (pParticle->isDead())
				{
					CIPHER_FREE(*poolAllocator, pParticle);
					pParticle = nullptr;
					pParticle = CIPHER_NEW(*poolAllocator, "PARTICLE_TEST", Particle);
					poolNewParticleCount++;

					pParticles.at(i) = pParticle;
				}
				else
				{
					pParticles.at(i)->Update(elapsedTime);
				}
			}

			elapsedTime = (clock() - frameStart) / (CLOCKS_PER_SEC / 1000);
			frameStart = clock();
		}
		pPoolSamples[j] = stopwatch.Stop();

		CIPHER_FREE(CIPHER.GlobalAllocator(), poolAllocator);

		printf("SETTING UP NEW KEYWORD TEST...\n");
		srand(seed);
		for (int i = 0; i < numParticles; i++)
		{
			pParticles.at(i) = new Particle();
		}
		int newNewParticleCount = 0;
		elapsedTime = 0;
		frameStart = clock();
		stopwatch.Start();
		while (newNewParticleCount <= numParticles)
		{
			for (int i = 0; i < numParticles; i++)
			{
				if (pParticles.at(i)->isDead())
				{
					delete pParticles.at(i);
					pParticles.at(i) = new Particle();
					newNewParticleCount++;
				}
				else
				{
					pParticles.at(i)->Update(elapsedTime);
				}
			}

			elapsedTime = (clock() - frameStart) / (CLOCKS_PER_SEC / 1000);
			frameStart = clock();
		}
		pNewSamples[j] = stopwatch.Stop();
		for (int i = 0; i < numParticles; i++)
		{
			delete pParticles.at(i);
			pParticles.at(i) = nullptr;
		}
	}

	logger.AddValue(numParticles);
	logger.AddValue(IAllocatorProfiler::CalculateAverageValue(pNewSamples, NUM_SAMPLE));
	logger.AddValue(IAllocatorProfiler::CalculateAverageValue(pPoolSamples, NUM_SAMPLE));
}

void FullSpeedTest(Stopwatch& stopwatch, CSVLogger& logger, int numAllocations)
{
	long double newAllocateAvg = NewKeywordProfiler::AllocateOnlyTime(stopwatch, numAllocations);
	long double newDeallocateAvg = NewKeywordProfiler::DeallocateOnlyTime(stopwatch, numAllocations);
	long double linearAllocateAvg = LinearAllocatorProfiler::AllocateOnlyTime(stopwatch, numAllocations);
	long double linearDeallocateAvg = LinearAllocatorProfiler::DeallocateOnlyTime(stopwatch, numAllocations);
	long double stackAllocateAvg = StackAllocatorProfiler::AllocateOnlyTime(stopwatch, numAllocations);
	long double stackDeallocateAvg = StackAllocatorProfiler::DeallocateOnlyTime(stopwatch, numAllocations);
	long double poolAllocateAvg = PoolAllocatorProfiler::AllocateOnlyTime(stopwatch, numAllocations);
	long double poolDeallocateAvg = PoolAllocatorProfiler::DeallocateOnlyTime(stopwatch, numAllocations);

	//printf("\nALLOCATING AND DEALLOCATING %i BYTES AND AVERAGED OVER %i SAMPLES\n", NUM_ALLOCS, NUM_SAMPLE);
	//printf("\nNEW KEYWORD\n\t ALLOC AVG TIME: %fms\tDEALLOC AVG TIME: %fms", newAllocateAvg, newDeallocateAvg);
	//printf("\nLINEAR ALLOCATOR\n\t ALLOC AVG TIME: %fms\tDEALLOC AVG TIME: %fms", linearAllocateAvg, linearDeallocateAvg);
	//printf("\nSTACK ALLOCATOR\n\t ALLOC AVG TIME: %fms\tDEALLOC AVG TIME: %fms", stackAllocateAvg, stackDeallocateAvg);
	//printf("\nPOOL ALLOCATOR\n\t ALLOC AVG TIME: %fms\t DEALLOC AVG TIME: %fms", poolAllocateAvg, poolDeallocateAvg);

	logger.AddValue("NUMBER OF ALLOCATIONS");
	logger.AddValue(numAllocations);
	logger.AddNewLine();
	logger.AddNewLine();

	//log allocation times
	logger.AddValue("NEW ALLOC");
	logger.AddValue(newAllocateAvg);
	logger.AddNewLine();
	logger.AddValue("LINEAR ALLOC");
	logger.AddValue(linearAllocateAvg);
	logger.AddNewLine();
	logger.AddValue("STACK ALLOC");
	logger.AddValue(stackAllocateAvg);
	logger.AddNewLine();
	logger.AddValue("POOL ALLOC");
	logger.AddValue(poolAllocateAvg);
	logger.AddNewLine();
	logger.AddNewLine();

	//log de-allocation times
	logger.AddValue("LINEAR DEALLOC");
	logger.AddValue(linearDeallocateAvg);
	logger.AddNewLine();
	logger.AddValue("NEW DEALLOC");
	logger.AddValue(newDeallocateAvg);
	logger.AddNewLine();
	logger.AddValue("STACK DEALLOC");
	logger.AddValue(stackDeallocateAvg);
	logger.AddNewLine();
	logger.AddValue("POOL DEALLOC");
	logger.AddValue(poolDeallocateAvg);
	logger.AddNewLine();
	logger.AddNewLine();
	logger.AddNewLine();
}

void ShowMenu(Stopwatch& stopwatch)
{
	int testRunTimes = 50;
	system("cls");
	cin.clear();
	printf("+==========================================================+\n");
	printf("|                       Cipher Tests                       |\n");
	printf("|==========================================================|\n");
	printf("|1. Full Speed Test             2.Single Frame Allocator   |\n");
	printf("|3. Bullet Pool                                            |\n");
	printf("|4. Run All (TAKES LONG TIME)   0. Quit                    |\n");
	printf("+----------------------------------------------------------+\n");

	printf("\nPlease Choose a test to run: ");
	int input = 0;
	cin >> input;

	CSVLogger* poolShowcaselogger = new CSVLogger("PoolAllocatorResults.csv");
	CSVLogger* fullSpeedLogger = new CSVLogger("FullSpeedResults.csv");
	CSVLogger* linearShowcaseLogger =  new CSVLogger("LinearAllocatorResults.csv");

	poolShowcaselogger->AddValue("SIZE OF ALLOCATIONS");
	poolShowcaselogger->AddValue(sizeof(DataType));
	poolShowcaselogger->AddValue("NUMBER OF SAMPLES");
	poolShowcaselogger->AddValue(NUM_SAMPLE);
	poolShowcaselogger->AddValue("NUMBER OF TESTS");
	poolShowcaselogger->AddValue(testRunTimes);
	poolShowcaselogger->AddNewLine();

	fullSpeedLogger->AddValue("SIZE OF ALLOCATIONS");
	fullSpeedLogger->AddValue(sizeof(DataType));
	fullSpeedLogger->AddValue("NUMBER OF SAMPLES");
	fullSpeedLogger->AddValue(NUM_SAMPLE);
	fullSpeedLogger->AddValue("NUMBER OF TESTS");
	fullSpeedLogger->AddValue(testRunTimes);
	fullSpeedLogger->AddNewLine();

	linearShowcaseLogger->AddValue("SIZE OF ALLOCATIONS");
	linearShowcaseLogger->AddValue(sizeof(DataType));
	linearShowcaseLogger->AddValue("NUMBER OF SAMPLES");
	linearShowcaseLogger->AddValue(NUM_SAMPLE);
	linearShowcaseLogger->AddValue("NUMBER OF TESTS");
	linearShowcaseLogger->AddValue(testRunTimes);
	linearShowcaseLogger->AddNewLine();

	switch (input)
	{
		case 0:
			return;
		case 1:
			system("cls");
			for (int i = 1; i <= testRunTimes; i++)
			{
				printf("Test #: %i\n", i);
				FullSpeedTest(stopwatch, *fullSpeedLogger, NUM_ALLOCS * i);
			}
			break;
		case 2:
			system("cls");
			for (int i = 1; i <= testRunTimes; i++)
			{
				PerformLinearAllocatorShowcase(stopwatch, *linearShowcaseLogger, NUM_ALLOCS * i);
			}			
			break;
		case 3:
			system("cls");
			poolShowcaselogger->AddValue("Amount of Particles");
			poolShowcaselogger->AddValue("New Keyword Allocations");
			poolShowcaselogger->AddValue("Pool Allocator Allocations");
			poolShowcaselogger->AddNewLine();

			for (int i = 1; i <= testRunTimes; i++)
			{
				PerformPoolAllocatorShowcase(stopwatch, *poolShowcaselogger, i * NUM_ALLOCS);
				poolShowcaselogger->AddNewLine();
			}			
			break;
		case 4:
			system("cls");
			for (int i = 1; i <= testRunTimes; i++)
			{
				FullSpeedTest(stopwatch, *fullSpeedLogger, NUM_ALLOCS * i);
			}

			system("cls");
			for (int i = 1; i <= testRunTimes; i++)
			{
				PerformLinearAllocatorShowcase(stopwatch, *linearShowcaseLogger, NUM_ALLOCS * i);
			}

			system("cls");
			poolShowcaselogger->AddValue("Amount of Particles");
			poolShowcaselogger->AddValue("New Keyword Allocations");
			poolShowcaselogger->AddValue("Pool Allocator Allocations");
			poolShowcaselogger->AddNewLine();

			for (int i = 1; i <= testRunTimes; i++)
			{
				PerformPoolAllocatorShowcase(stopwatch, *poolShowcaselogger, i * NUM_ALLOCS);
				poolShowcaselogger->AddNewLine();
			}
			break;
		default:
			system("cls");
			printf("Please enter a valid selection. 0 to quit.");
			Sleep(1000);
			break;
	}

	printf("\nEnter to return...");
	char temp;
	cin.ignore();
	cin.get(temp);
	ShowMenu(stopwatch);
}

void main()
{
	CIPHER.Initialise((NUM_ALLOCS * sizeof(DataType)) + 100);
	Stopwatch* stopwatch = new Stopwatch();
	ShowMenu(*stopwatch);
}