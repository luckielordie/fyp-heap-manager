#include <Stopwatch.h>

using namespace std;

Stopwatch::Stopwatch()
{
	QueryPerformanceFrequency((LARGE_INTEGER*)&ticksPerSecond);
}

void Stopwatch::Start()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&start);
	//printf("\tStopwatch Starting...\n");
}

long double Stopwatch::Stop()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&end);
	long double elapsed = (end - start) / (ticksPerSecond / 1000);
	//printf("\tStopwatch Stopped at %fms.\n", elapsed);

	return elapsed;
}