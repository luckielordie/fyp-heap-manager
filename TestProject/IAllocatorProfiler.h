#ifndef IALLOCATORPROFILER_H
#define IALLOCATORPROFILER_H

#include "TestDefines.h"
#include "Stopwatch.h"

class IAllocatorProfiler
{
public:

	static long double CalculateAverageValue(long double* sampleArray, int numSamples)
	{
		//work out average of all the samples 
		float average = 0.0f;
		for (int i = 0; i < numSamples; i++)
		{
			average += sampleArray[i];
		}

		average /= numSamples;

		return average;
	}

	static long double CalculateAverageValue(int* sampleArray, int numSamples)
	{
		//work out average of all the samples 
		float average = 0.0f;
		for (int i = 0; i < numSamples; i++)
		{
			average += sampleArray[i];
		}

		average /= numSamples;

		return average;
	}

	virtual long double AllocateAndDeallocateTime(Stopwatch& stopwatch, int numAllocations);
	virtual long double AllocateOnlyTime(Stopwatch& stopwatch, int numAllocations);
	virtual long double DeallocateOnlyTime(Stopwatch& stopwatch, int numAllocations);
};

#endif