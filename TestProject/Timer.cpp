#include <Timer.h>

using namespace std;
using namespace std::chrono;

Timer::Timer(function<void()> callback, int millsecondsTime, bool repeat)
{
	m_timeMilliseconds = millsecondsTime;
	m_callbackFunction = callback;
	m_repeat = repeat;
}

void Timer::Start()
{
	m_startTime = high_resolution_clock::now();
}

void Timer::Update()
{
	m_currentTime = high_resolution_clock::now();

	if (((duration_cast<duration<int, ratio<1, 1000>>>(m_currentTime - m_startTime).count()) % m_timeMilliseconds == 0) && m_repeat)
	{
		if (m_repeat && !m_calledOnce)
		{
			m_callbackFunction();
		}
		else
		{
			m_calledOnce = true;
			m_callbackFunction();
		}
	}
}