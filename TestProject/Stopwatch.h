#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <string>

#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>

class Stopwatch
{
private:
	long double ticksPerSecond;
	long double start;
	long double end;
public:
	Stopwatch();
	void Start();
	long double Stop();
};

#endif