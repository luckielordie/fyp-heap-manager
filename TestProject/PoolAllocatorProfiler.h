#ifndef POOLALLOCATORPROFILER_H
#define POOLALLOCATORPROFILER_H

#include <IAllocatorProfiler.h>
#include <CipherManager.h>

class PoolAllocatorProfiler : IAllocatorProfiler
{
public:
	static long double AllocateAndDeallocateTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& poolAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::PoolAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType), sizeof(DataType), __alignof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nPOOL ALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(poolAllocator, "POOL_TEST", DataType);
			}

			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				Cipher::CIPHER_FREE(poolAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			//end probe here
			pSamples[i] = stopwatch.Stop();
			delete[] ptr;
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &poolAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double AllocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& poolAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::PoolAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType), sizeof(DataType), __alignof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nPOOL ALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];
			stopwatch.Start();
			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(poolAllocator, "POOL_TEST", DataType);
			}
			pSamples[i] = stopwatch.Stop();

			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				Cipher::CIPHER_FREE(poolAllocator, ptr[j]);
				ptr[j] = nullptr;
			}

			delete[] ptr;
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &poolAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}

	static long double DeallocateOnlyTime(Stopwatch& stopwatch, int numAllocations)
	{
		Cipher::Allocators::Allocator& poolAllocator = *Cipher::CIPHER.CreateAllocator<Cipher::Allocators::PoolAllocator>(Cipher::CIPHER.GlobalAllocator(), numAllocations * sizeof(DataType), sizeof(DataType), __alignof(DataType));

		long double* pSamples = new long double[NUM_SAMPLE];
		//printf("\nPOOL DEALLOC START\n");
		for (int i = 0; i < NUM_SAMPLE; i++)
		{
			//printf("\n\tSAMPLE %i\n", i + 1);
			//time probe here
			DataType** ptr = new DataType*[numAllocations];

			for (int j = 0; j < numAllocations; j++)
			{
				ptr[j] = Cipher::CIPHER_NEW(poolAllocator, "POOL_TEST", DataType);
			}

			stopwatch.Start();
			//free all allocated memory
			for (int j = 0; j < numAllocations; j++)
			{
				Cipher::CIPHER_FREE(poolAllocator, ptr[j]);
				ptr[j] = nullptr;
			}
			
			//end probe here
			pSamples[i] = stopwatch.Stop();
			delete[] ptr;
		}
		Cipher::CIPHER_FREE(Cipher::CIPHER.GlobalAllocator(), &poolAllocator);
		static long double average = IAllocatorProfiler::CalculateAverageValue(pSamples, NUM_SAMPLE);
		delete[] pSamples;
		return average;
	}
};

#endif