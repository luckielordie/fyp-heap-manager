# README #

This solution comes with three projects. The Cipher DLL is the allocator and provides functions for allocating on the heap. The Test Project is a project that uses the Cipher DLL to time and profile Cipher. The Example project contains code examples of how to create your own allocator and the usage of the Cipher DLL. The solution also contains the beginnings of a tool to view the snapshot.xml that is output by the DLL during Debug mode.

The writeup for this project can be found at this link. [Paper](https://onedrive.live.com/redir?resid=4B91DC9AB2D6EDD4!329&authkey=!AP8ONh1tXGe4oeA&ithint=file%2cpdf)