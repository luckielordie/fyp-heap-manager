#include <iostream>

#define _WINSOCKAPI_
//#define CIPHER_ONLY_HEAP
#include <CipherManager.h>

#include "Vector.h"
#include "Test.h"
#include "Stopwatch.h"

using namespace Cipher;
using namespace Cipher::Allocators;
using namespace ExampleProject;

void VectorExample()
{
	CIPHER.Initialise(10000);
	Stopwatch stopwatch = Stopwatch();
	Vector<Foo>* fooVector = CIPHER_NEW(&CIPHER.GlobalAllocator(), "Vector", Vector<Foo>, 30);
	for (int i = 0; i < fooVector->Size(); i++)
	{
		Foo* foo = fooVector->At(i);
		*foo = Foo(i, i + 1);
	}
}

void SnapshotExample()
{
	CIPHER.Initialise(10000);
	int numItems = 50;
	LinearAllocator* linearAllocator = CIPHER.CreateAllocator<LinearAllocator>(CIPHER.GlobalAllocator(), numItems * sizeof(Foo));

	Foo** foo = new Foo*[numItems];

	for (int j = 0; j < 2; j++)
	{
		CIPHER.TakeSnapshot();
		CIPHER.SnapShotStart();
		
		for (int i = 0; i < numItems; i++)
		{
			foo[i] = CIPHER_NEW(linearAllocator, "GameThings", Foo, 1, 1);
		}
		CIPHER.SnapShotEnd();
		for (int i = 0; i < numItems; i++)
		{
			CIPHER_FREE(linearAllocator, foo[i]);
		}
	}
}

void main()
{
	SnapshotExample();

	std::cout << "DONE";
	int f = 0;
	std::cin >> f;
}