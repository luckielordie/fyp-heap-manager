#pragma once

#include <string>
#include <iostream>

class Foo
{
private: 
	int a;
	int b;

public:
	Foo(int a, int b)
	{
		this->a = a;
		this->b = b;
		printf("FOO CONSTRUCTOR CALLED\n");
	}

	Foo()
	{
		a = 0;
		b = 0;
	}

	void Print()
	{
		printf("Num 1: %i, Num 2: %i\n", a, b);
	}

	~Foo()
	{
		printf("FOO DESTRUCTOR CALLED\n");
	}
};

class Bar
{
private: 
	std::string a;

public:
	Bar(std::string a)
	{
		this->a = a;
		printf("FOO CONSTRUCTOR CALLED\n");
	}

	Bar()
	{
		a = "YOYO";
	}

	~Bar()
	{
		printf("BAR DESTRUCTOR CALLED\n");
	}
};