#ifndef EXAMPLEPROJECT_VECTOR_H
#define EXAMPLEPROJECT_VECTOR_H

#include <CipherManager.h>

namespace ExampleProject
{
	template<class VectorType>
	class Vector
	{
	private: 
		Cipher::Allocators::PoolAllocator* m_pAllocator;
		int m_NumObjects;


	public:
		Vector(int numObjects)
		{
			m_NumObjects = numObjects;
			int allocatorSize = (sizeof(VectorType) + __alignof(VectorType)) * numObjects;
			m_pAllocator = CIPHER.CreateAllocator<Cipher::Allocators::PoolAllocator>(CIPHER.GlobalAllocator(), allocatorSize, sizeof(VectorType), __alignof(VectorType));
		}

		VectorType* At(int index)
		{
			if (index > m_NumObjects + 1) return nullptr;

			void* topPtr = (void*)m_pAllocator->m_pTopHeader->pMem;

			return (VectorType*)(((uintptr_t)topPtr) + ((sizeof(VectorType) + __alignof(VectorType)) * index));
		}

		int Size()
		{
			return m_NumObjects;
		}

	};
}

#endif