#include <PageManager.h>

using namespace Cipher::PageManagement;
using namespace Cipher::PageManagement::Allocators;
using namespace std;

PageManager::PageManager()
{
	m_pPageMap = new map<string, Page*>();
}

PageManager::~PageManager()
{
	FreeAllPages();

	delete m_pPageMap;
	m_pPageMap = nullptr;
}

void PageManager::FreeAllPages()
{
	for (std::map<string, Page*>::iterator it = m_pPageMap->begin(); it != m_pPageMap->end(); it++)
	{
		delete (*it).second;
		(*it).second = nullptr;
	}

	m_pPageMap->clear();
}

void PageManager::FreePage(string pageName)
{
	Page* pageToRemove = m_pPageMap->at(pageName);
	m_pPageMap->erase(pageName);
	delete pageToRemove;
	pageToRemove = nullptr;
}

void* PageManager::Allocate(string pageName, size_t allocationSize, size_t allocationAlignment)
{
	Page* allocationPage = m_pPageMap->at(pageName);//this->GetPagePointer(pageName);

	if (allocationPage != nullptr)
	{
		uintptr_t ptr = allocationPage->Allocate(allocationSize, allocationAlignment);

		if (ptr == 0)
		{
			//LOG FAILED ATTEMPT
			return nullptr;
		}

		return reinterpret_cast<void*>(ptr);
	}

	return nullptr;
}

void PageManager::Free(void* pointer)
{
	Page* page = this->GetPagePointer(pointer);
	if (page != nullptr)
	{
		page->Free(pointer);
	}
	else
	{
		//INVALID ADDRESS
	}
}

Page* PageManager::GetPagePointer(string pageName)
{
	/*for (std::vector<Page*>::iterator it = m_pPageList->begin(); it != m_pPageList->end(); it++)
	{
		if ((*it)->GetPageName() == pageName) return *it;
	}*/
	
	return nullptr;
}

Page* PageManager::GetPagePointer(void* pointer)
{
	for(std::map<string, Page*>::iterator it = m_pPageMap->begin(); it != m_pPageMap->end(); it++)
	{
		Page* page = (*it).second;
		if (page->IsPointerInPage(pointer))
		{
			return page;
		}
	}

	return nullptr;
}
