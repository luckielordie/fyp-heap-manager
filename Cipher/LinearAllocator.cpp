#include <LinearAllocator.h>

using namespace Cipher::Allocators;

LinearAllocator::LinearAllocator(void* memTop, size_t memSize) : Allocator(memTop, memSize)
{
	m_pCurrent = m_pMemTop;
}

LinearAllocator::~LinearAllocator()
{

}

inline void* LinearAllocator::Allocate(size_t size, uint8_t alignment)
{
	size_t adjustment = 0;

	if (alignment > 0)
	{
		adjustment = Allocator::GetAdjustment(m_pCurrent, alignment);
	}

	//check size of allocation not out of bounds
	if (m_pCurrent + adjustment + size >
		m_pMemTop + m_memSize)
	{
		return 0;
	}

	//align the address
	uintptr_t address = m_pCurrent + adjustment;
	//add space onto page
	m_pCurrent = address + size;

	return (void*)address;
}

inline void LinearAllocator::Free(void* ptr)
{
	m_pCurrent = m_pMemTop;
}