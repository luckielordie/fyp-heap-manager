#ifndef CIPHER_ALLOCATORS_ALLOCATOR_H
#define CIPHER_ALLOCATORS_ALLOCATOR_H

#include <assert.h>
#include <cstdint>
#include <sstream>

#include <OutputWindowMacro.h>

namespace Cipher
{
	namespace Allocators
	{
		class __declspec(dllexport) Allocator
		{
		protected:
			uintptr_t m_pMemTop;
			size_t m_memSize;

		protected:
			static size_t GetAdjustment(const uintptr_t address, size_t alignment);
			static size_t GetAdjustment(const uintptr_t address, size_t alignement, size_t headerSize);

		public:
			Allocator(void* pMemTop, size_t memSize);
			virtual inline void* Allocate(size_t size, uint8_t alignment) = 0;
			virtual inline void Free(void* p) = 0;
		};
	}
}
#endif