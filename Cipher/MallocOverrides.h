#ifndef MALLOCOVERRIDES_H
#define MALLOCOVERRIDES_H

#include <assert.h>
#include <stdlib.h>

void* CipherMalloc(int size)
{
	assert(false && "MALLOC USE DETECTED: Are you using new keywords anywhere? Please only use CIPHER_NEW to allocate memory!");
	return nullptr;
}

void CipherFree(void* ptr)
{
	assert(false && "FREE USE DETECTED: Are you using the delete keyword anywhere? Please only use CIPHER_FREE to deallocate memory!");
}

void* CipherRealloc(void* ptr, int size)
{
	assert(false && "REALLOC USE DETECTED: Are you using new keywords anywhere? Please only use CIPHER_NEW to allocate memory!");
	return nullptr;
}

#define malloc(size) CipherMalloc(size)
#define free(ptr) CipherFree(ptr)
#define realloc(ptr, size) CipherRealloc(ptr, size)

#endif