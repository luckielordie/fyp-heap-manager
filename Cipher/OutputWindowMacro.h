#ifndef OUTPUTWINDOWMACRO
#define OUTPUTWINDOWMACRO
#include <windows.h>
#include <iostream>
#include <sstream>

#if defined(DEBUG) | defined(_DEBUG)
#ifndef CIPHER_ERROR
#define CIPHER_ERROR(x)																											\
		{																														\
			std::wostringstream os_;																							\
			os_ << x;																											\
			OutputDebugString(os_.str().c_str());																				\
		}															
#endif
#else
#ifndef CIPHER_ERROR
#define CIPHER_ERROR(x) (x)
#endif
#endif
#endif