#include <Allocator.h>

using namespace Cipher::Allocators;

Allocator::Allocator(void* memTop, size_t memSize)
{
	m_pMemTop = (uintptr_t)memTop;
	m_memSize = memSize;
}

size_t Allocator::GetAdjustment(const uintptr_t address, size_t alignment)
{
	size_t mask = alignment - 1;
	size_t misalignment = address & mask;
	size_t adjustment = alignment - misalignment;
	
	if (adjustment == alignment) return 0;
	return adjustment;
}

size_t Allocator::GetAdjustment(const uintptr_t address, size_t alignment, size_t headerSize)
{
	size_t adjustment = Allocator::GetAdjustment(address, alignment);

	if (adjustment < headerSize)
	{
		headerSize -= adjustment;

		adjustment += alignment * (headerSize / alignment);

		if (headerSize % alignment > 0) adjustment += alignment;
	}

	return adjustment;
}
