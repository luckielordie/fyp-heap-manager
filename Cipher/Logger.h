#ifndef CIPHER_LOGGING_LOGGER_H
#define CIPHER_LOGGING_LOGGER_H

#include <string>
#include <vector>

namespace Cipher
{
	namespace Logging
	{
		class Logger
		{
		protected:
			std::vector<char>* m_pDataBuffer;
		public:
			Logger()
			{
				m_pDataBuffer = new std::vector<char>();
			}

			void AddValue(std::string string)
			{
				for (unsigned int i = 0; i < string.size(); i++)
				{
					m_pDataBuffer->push_back(string.at(i));
				}
			}

			void AddValue(char* charArray)
			{
				size_t length = strlen(charArray);

				for (size_t i = 0; i < length; i++)
				{
					m_pDataBuffer->push_back(charArray[i]);
				}
			}

			void AddValue(int number)
			{
				m_pDataBuffer->push_back(number);
			}

			virtual void SaveLog() = 0;
		};
	}
}

#endif