#include <CipherManager.h>

using namespace Cipher;
using namespace Cipher::Allocators;
using namespace Cipher::Logging;
using namespace std;

CipherManager::CipherManager()
{
}

CipherManager::~CipherManager()
{

}

CipherManager& CipherManager::Instance()
{
	static CipherManager instance;
	return instance;
}

void CipherManager::Initialise(size_t globalAllocatorSizeBytes)
{
#if _DEBUG
	m_pLogger = new XMLLogger();
#endif
	m_pSystemAllocators = new vector<Allocator*>();

	//Create Global Allocator
	void* pMem = VirtualAlloc(NULL, globalAllocatorSizeBytes, MEM_COMMIT, PAGE_READWRITE);
	m_pGlobalAllocator = new StackAllocator(pMem, globalAllocatorSizeBytes);
}

void CipherManager::Cleanup()
{
	
}

void CipherManager::TakeSnapshot()
{
	m_takeSnapshot = true;
}

void CipherManager::SnapShotStart()
{
#if _DEBUG
	//check for end Probe
	if (m_takeSnapshot == true)
	{
		m_logSnapshot = true;
		return;
	}
	else
	{
		m_logSnapshot = false;
	}
#endif
}

void CipherManager::SnapShotEnd()
{
#if _DEBUG
	if (m_logSnapshot)
	{
		m_pLogger->SaveLog();
	}

	m_logSnapshot = false;
	m_takeSnapshot = false;
#endif
}

Allocator& CipherManager::GlobalAllocator()
{
	return *m_pGlobalAllocator;
}

void CipherManager::LogAllocation(const char* allocationType, size_t allocationSize, size_t allocationAlignment, const char* fileName, const int lineNumber, const char* functionName)
{
	if (m_logSnapshot == true)
	{
		m_pLogger->AddAllocation(allocationType, allocationSize, allocationAlignment, string(fileName), lineNumber, string(functionName));
	}
}