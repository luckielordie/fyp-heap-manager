#ifndef CIPHER_LOGGING_NETWORKLOGGER_H
#define CIPHER_LOGGING_NETWORKLOGGER_H

#include <Logger.h>

namespace asio
{
	namespace ip
	{
		class tcp;
	}
	template<typename R>
	class stream_socket_service;

	template<typename T, typename Q>
	class basic_stream_socket;
}


namespace Cipher
{
	namespace Logging
	{
		class NetworkLogger : public Logger
		{
		private:
			std::vector<char>* m_pReadBuffer;
			asio::basic_stream_socket<asio::ip::tcp, asio::stream_socket_service<asio::ip::tcp>>* m_pSocket;
		public:
			NetworkLogger();
			void SaveLog();
		};
	}
}

#endif