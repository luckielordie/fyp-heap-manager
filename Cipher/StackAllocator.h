#ifndef CIPHER_ALLOCATORS_STACKALLOCATOR_H
#define CIPHER_ALLOCATORS_STACKALLOCATOR_H

#include <Allocator.h>
#include <FreeList.h>

namespace Cipher
{
	namespace Allocators
	{
		class __declspec(dllexport) StackAllocator : public Allocator
		{
		private:
			struct AllocHeader
			{
				AllocHeader* m_pPreviousHeader;
				uintptr_t m_pData;
				size_t adjustment;
			};

			FreeList<AllocHeader>* m_pHeaderList;
			AllocHeader* m_pTopStackHeader;

			uintptr_t m_pCurrentPosition;
		public:
			StackAllocator(void* memTop, size_t memSize);
			~StackAllocator();
			inline void* Allocate(size_t size, uint8_t alignment);
			inline void Free(void* ptr);
		};
	}
}

#endif