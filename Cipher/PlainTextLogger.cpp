#include <PlainTextLogger.h>

using namespace std;
using namespace Cipher::Logging;

PlainTextLogger::PlainTextLogger() : Logger()
{

}

void PlainTextLogger::SaveLog()
{
	ofstream myFile;
	myFile.open("OUTPUT.txt");
	m_pDataBuffer->push_back('\0');
	myFile << m_pDataBuffer->data();
	myFile.close();
}