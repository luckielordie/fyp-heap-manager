#include <XMLLogger.h>
#include <TinyXML-2\tinyxml2.h>

using namespace Cipher::Logging;
using namespace std;
using namespace tinyxml2;

XMLLogger::XMLLogger()
{
	m_pDocument = new XMLDocument();
	m_pNodeBuffer = new vector<XMLElement*>();
	m_pDocumentRoot = m_pDocument->NewElement("Root");
}

void XMLLogger::AddAllocation(std::string allocationType, size_t allocationSize,
	size_t allocationAlignment, std::string fileName, int lineNumber, std::string functionName)
{
	if (m_newSnapshot == true)
	{
		m_pRootElement = m_pDocument->NewElement("Snapshot");
		m_pRootElement->SetAttribute("id", m_snapshotNumber++);
		m_newSnapshot = false;
	}

	//create element and fill attributes
	XMLElement* allocation = m_pDocument->NewElement("Allocation");
	allocation->SetAttribute("allocationType", allocationType.c_str());
	allocation->SetAttribute("allocationSize", (unsigned int)allocationSize);
	allocation->SetAttribute("allocationAlignment", (unsigned int)allocationAlignment);
	allocation->SetAttribute("fileName", fileName.c_str());
	allocation->SetAttribute("lineNumber", lineNumber);
	allocation->SetAttribute("functionName", functionName.c_str());

	//push back to buffer
	m_pNodeBuffer->push_back(allocation);
}

void XMLLogger::SaveLog()
{
	for (unsigned int i = 0; i < m_pNodeBuffer->size(); i++)
	{
		m_pRootElement->InsertEndChild(m_pNodeBuffer->at(i));
	}

	m_pNodeBuffer->clear();

	m_pDocumentRoot->InsertEndChild(m_pRootElement);
	m_pDocument->InsertFirstChild(m_pDocumentRoot);
	m_pDocument->SaveFile("snapshot.xml");
	m_newSnapshot = true;
}