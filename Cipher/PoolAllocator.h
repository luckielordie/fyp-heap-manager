#ifndef CIPHER_ALLOCATORS_POOLALLOCATOR_H
#define CIPHER_ALLOCATORS_POOLALLOCATOR_H
#include <Allocator.h>
#include <FreeList.h>

namespace Cipher
{
	namespace Allocators
	{
		class __declspec(dllexport) PoolAllocator : public Allocator
		{
		private:
			size_t m_objectSize;
			uint8_t m_objectAlignment;

			struct AllocHeader
			{
				uintptr_t pMem;
				AllocHeader* nextHeader;
			};

			FreeList<AllocHeader>* m_pFreeList;

		public:
			AllocHeader* m_pTopHeader;
			PoolAllocator(void* pMem, size_t size, size_t objectSize, size_t objectAlignment);
			~PoolAllocator();

			void* Allocate(size_t size, uint8_t alignment);
			void Free(void* ptr);
		};
	}
}


#endif