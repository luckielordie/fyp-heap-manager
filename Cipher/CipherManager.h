#ifndef CIPHER_CIPHERMANAGER_H
#define CIPHER_CIPHERMANAGER_H

#include <string>
#include <NetworkLogger.h>
#include <XMLLogger.h>
#include <vector>

#include <Allocator.h>
#include <LinearAllocator.h>
#include <StackAllocator.h>
#include <PoolAllocator.h>

#include <sstream>

#if defined CIPHER_ONLY_HEAP
#include <MallocOverrides.h>
#endif

namespace Cipher
{
	class __declspec(dllexport) CipherManager
	{
	private:
		bool m_logSnapshot = false;
		bool m_takeSnapshot = false;
		Logging::XMLLogger* m_pLogger;

		Allocators::Allocator* m_pGlobalAllocator;
		std::vector<Allocators::Allocator*>* m_pSystemAllocators;

	private:
		CipherManager();
		~CipherManager();
		CipherManager(CipherManager const& m){};
		void operator =(CipherManager const&){};

		void LogAllocation(const char* allocationType, size_t allocationSize, size_t allocationAlignment, const char* fileName, const int lineNumber, const char* functionName);

	public:
		static CipherManager& Instance();
		Allocators::Allocator& GlobalAllocator();	

		void SnapShotStart();
		void SnapShotEnd();
		void TakeSnapshot();

		void Initialise(size_t globalAllocatorSize);
		void Cleanup();

		
		template<class T, typename ... arguments>
		T* CreateAllocator(Allocators::Allocator& parentAllocator, size_t allocatorSize, arguments... args)
		{
			void* ptr = parentAllocator.Allocate(allocatorSize + sizeof(T), __alignof(T));
			T* newAllocator = new(ptr)T((void*)((uintptr_t)ptr + sizeof(T)), allocatorSize, args...);
			m_pSystemAllocators->push_back(newAllocator);

			return newAllocator;
		}

		template<class T, typename... arguments>
		inline T* CipherNew(Allocators::Allocator* allocator, const char* allocationType, const char* fileName, const int lineNumber, const char* functionName, arguments... args)
		{
#ifdef _DEBUG
			this->LogAllocation(allocationType, sizeof(T), __alignof(T), fileName, lineNumber, functionName);
#endif
			return new(allocator->Allocate(sizeof(T), __alignof(T))) T(args...);
		}

		template<class T>
		inline void CipherDelete(Allocators::Allocator* allocator, T* object)
		{
			object->~T();
			allocator->Free((void*)object);
		}
	};

#define CIPHER CipherManager::Instance()
#define CIPHER_NEW(ALLOCATOR, TYPE, CLASS, ...) CIPHER.CipherNew<CLASS>(ALLOCATOR, TYPE, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#define CIPHER_FREE(ALLOCATOR, OBJECT) CIPHER.CipherDelete(ALLOCATOR, OBJECT)
}
#endif