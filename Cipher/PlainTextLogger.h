#ifndef CIPHER_LOGGING_PLAINTEXTLOGGER_H
#define CIPHER_LOGGING_PLAINTEXTLOGGER_H

#include <Logger.h>
#include <string>
#include <iostream>
#include <fstream>

namespace Cipher
{
	namespace Logging
	{
		class PlainTextLogger : public Logger
		{
		private:
		public:
			PlainTextLogger();
			void SaveLog();
		};
	}
}

#endif