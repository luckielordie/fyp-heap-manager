#ifndef CIPHER_PAGEMANAGEMENT_ALLOCATORS_DOUBLEENDEDSTACKALLOCATOR_H
#define CIPHER_PAGEMANAGEMENT_ALLOCATORS_DOUBLEENDEDSTACKALLOCATOR_H

#include <Allocator.h>

namespace Cipher
{
	namespace PageManagement
	{
		namespace Allocators
		{
			class __declspec(dllexport) DoubleEndedStackAllocator : public Allocator
			{
			private:
			public:
				DoubleEndedStackAllocator(Cipher::PageManagement::Page* page);
				inline uintptr_t Allocate(size_t size, size_t alignment);
				inline void Free(void* ptr);
			};
		}
	}
}

#endif