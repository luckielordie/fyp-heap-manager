#include <DoubleEndedStackAllocator.h>

using namespace Cipher::PageManagement;
using namespace Cipher::PageManagement::Allocators;

DoubleEndedStackAllocator::DoubleEndedStackAllocator(Page* page) : Allocator(page)
{

}

inline uintptr_t DoubleEndedStackAllocator::Allocate(size_t size, size_t alignment)
{
	return m_pPage->GetPageTopPointer();
}

inline void DoubleEndedStackAllocator::Free(void* ptr)
{

}