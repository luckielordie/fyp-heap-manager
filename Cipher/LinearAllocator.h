#ifndef CIPHER_ALLOCATORS_LINEARALLOCATOR_H
#define CIPHER_ALLOCATORS_LINEARALLOCATOR_H

#include <stdlib.h>
#include <Allocator.h>

namespace Cipher
{
	namespace Allocators
	{
		class __declspec(dllexport) LinearAllocator : public Allocator
		{
		private:
			uintptr_t m_pCurrent;
		public:
			LinearAllocator(void* memTop, size_t memSize);
			inline void* Allocate(size_t size, uint8_t alignment);
			inline void Free(void* ptr);
			~LinearAllocator();
		};
	}
}

#endif