#include <PoolAllocator.h>

using namespace Cipher;
using namespace Cipher::Allocators;

PoolAllocator::PoolAllocator(void* pMem, size_t size, size_t objectSize, size_t objectAlignment) : Allocator(pMem, size)
{
	m_pFreeList = new FreeList<AllocHeader>();
	m_objectSize = objectSize;
	m_objectAlignment = objectAlignment;
	m_pTopHeader = nullptr;

	m_pMemTop = m_pMemTop;// +Allocator::GetAdjustment(m_pMemTop, objectAlignment);

	int numElements = m_memSize / m_objectSize;

	for (int i = 0; i < numElements; i++)
	{
		AllocHeader* newHeader = m_pFreeList->Alloc();
		newHeader->nextHeader = nullptr;
		newHeader->pMem = m_pMemTop + (i * objectSize);

		if (m_pTopHeader == nullptr)
		{			
			m_pTopHeader = newHeader;
		}
		else
		{
			newHeader->nextHeader = m_pTopHeader;
			m_pTopHeader = newHeader;
		}
	}
}

PoolAllocator::~PoolAllocator()
{
	delete m_pFreeList;
	m_pFreeList = nullptr;
	m_pTopHeader = false;
}

void* PoolAllocator::Allocate(size_t size, uint8_t alignment)
{
	AllocHeader* tempHeader = m_pTopHeader;
	
	if (tempHeader == nullptr)
	{
		assert(false && "POOL ALLOCATOR RUN OUT OF MEMORY");
		return nullptr;
	}
	
	void* address = (void*)tempHeader->pMem;
	m_pTopHeader = tempHeader->nextHeader;
	m_pFreeList->Free(tempHeader);

	return address;
}

void PoolAllocator::Free(void* ptr)
{
	AllocHeader* tempHeader = m_pFreeList->Alloc();
	uintptr_t pointer = (uintptr_t)ptr;

	tempHeader->pMem = pointer;
	tempHeader->nextHeader = m_pTopHeader;
	m_pTopHeader = tempHeader;
}

