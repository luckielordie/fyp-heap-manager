#ifndef CIPHER_PAGEMANAGEMENT_PAGEMANAGER_H
#define CIPHER_PAGEMANAGEMENT_PAGEMANAGER_H

#include <map>

//include allocator headers
#include <LinearAllocator.h>
#include <StackAllocator.h>

#include <stdlib.h>

namespace Cipher
{
	namespace PageManagement
	{
		class __declspec(dllexport) PageManager
		{
		private:
			//prevent allocation and deallocation from outside the class
			PageManager(PageManager const& m){};
			void operator =(PageManager const&){};

			std::map<std::string, Page*>* m_pPageMap;

			inline Page* GetPagePointer(std::string pageName);
			inline Page* GetPagePointer(void* pointer);
		public:
			PageManager();
			~PageManager();

			inline void* Allocate(std::string pageName, size_t allocationSize, size_t allocationAlignment);
			inline void Free(void* pointer);

			void FreeAllPages();
			void FreePage(std::string pageName);

			template<class Allocator>
			void CreatePage(std::string pageName, size_t pageSize)
			{
				Page* page = new Page(pageName, pageSize);
				page->SetAllocator(new Allocator(page));
				m_pPageMap->insert(std::pair<std::string, Page*>(pageName, page));
			}
		};
	}
}

#endif