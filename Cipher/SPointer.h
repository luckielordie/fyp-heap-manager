#ifndef CIPHER_SPOINTER_H
#define CIPHER_SPOINTER_H

#include <list>
#include <RefCounter.h>

namespace Cipher
{
	template <class T>
	class SPointer
	{
	private:
		static std::list<SPointer*>m_liveObjects;
		static std::list<SPointer*>m_deadObjects;

		static void MoveToDeadList(SPointer* pointer)
		{
			m_liveObjects.remove(pointer);
			m_deadObjects.push_back(pointer);
		}

	protected:
		RefCounter* m_pRefCounter;
		T* m_pObject;

	public:
		SPointer()
		{
			m_pRefCounter = new RefCounter();
			m_pObject = nullptr;
		}

		SPointer(T* objectPointer)
		{
			m_pRefCounter = new RefCounter();
			m_objectPointer = nullptr;
		}

		//copy cons
		SPointer(const SPointer& pointer)
		{
			//release current pointer
			if (m_pObject != nullptr) m_pRefCounter->Release();

			m_pObject = pointer.m_pObject;
			m_pRefCounter = pointer.m_pRefCounter;

			if (m_pObject != nullptr) m_pRefCounter->AddRef();
		}

		~SPointer()
		{
			if (m_pObject != nullptr) m_pRefCounter->Release();
			delete m_pRefCounter;
		}

		//overloading assignment operators
		void operator =(T* object)
		{
			this->m_pRefCounter->Reset();
			MoveToDeadList(this);
			this = new SPointer<T>(object);
		}

		void operator =(const SPointer<T>& pointer)
		{
			this->m_pRefCounter->Reset();
			MoveToDeadList(this);
			this = new SPointer(pointer);
		}

		T& operator *() const
		{
			return *m_pObject;
		}

		inline T* operator-> () const
		{
			return m_pObject;
		}

		operator T*() const
		{
			return m_pObject;
		}

		bool isValid() const
		{
			return (m_pObject != nullptr);
		}

		bool operator ==(const SPointer<T>& pointer) const
		{
			return (m_pObject == pointer.m_pObject);
		}

		bool operator ==(const T* object) const
		{
			return (m_pObject == object);
		}

		//GARBAGE COLLECTION
		static void DeleteDeadObjects()
		{
			for (std::list<SPointer*>::iterator it = m_deadObjects.begin(); it != m_deadObjects.end(); it++)
			{
				//delete obj
				delete(*it);
			}

			m_deadObjects.clear();
		}

		static void RemoveAllObjects(bool emitWarnings)
		{
			DeleteDeadObjects();
			for (std::list<SPointer*>::iterator it = m_liveObjects.begin();
				it != m_liveObjects.end(); it++)
			{
				SPointer*  object = *it;
				if (emitWarnings)
				{
					//log errors
				}

				delete object;
			}
		}
	};
}
#endif