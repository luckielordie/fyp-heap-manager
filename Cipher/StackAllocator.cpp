#include <StackAllocator.h>

using namespace Cipher::Allocators;

StackAllocator::StackAllocator(void* memTop, size_t memSize) : Allocator(memTop, memSize)
{
	m_pCurrentPosition = m_pMemTop;
	m_pHeaderList = new FreeList<AllocHeader>();
	m_pTopStackHeader = nullptr;
}

StackAllocator::~StackAllocator()
{
	delete m_pHeaderList;
	m_pHeaderList = nullptr;
	m_pTopStackHeader = nullptr;
}

inline void* StackAllocator::Allocate(size_t size, uint8_t alignment)
{
	size_t adjustment = Allocator::GetAdjustment(m_pCurrentPosition, alignment);		

	if (m_pCurrentPosition + adjustment + size >
		m_pMemTop + m_memSize)
	{
		//too big
		return nullptr;
	}

	uintptr_t alignedAddress = m_pCurrentPosition + adjustment;

	//create header on free list then link top ref to it
	AllocHeader* header = m_pHeaderList->Alloc();
	header->adjustment = adjustment + size;
	header->m_pPreviousHeader = m_pTopStackHeader;
	header->m_pData = alignedAddress;
	m_pTopStackHeader = header;
	
	m_pCurrentPosition = alignedAddress + size;
	
	return (void*)alignedAddress;
}

inline void StackAllocator::Free(void* ptr)
{
	uintptr_t address = (uintptr_t)ptr;
	if (address == m_pTopStackHeader->m_pData)
	{
		m_pCurrentPosition -= m_pTopStackHeader->adjustment;

		//free header
		AllocHeader* tempTop = m_pTopStackHeader;
		m_pTopStackHeader = m_pTopStackHeader->m_pPreviousHeader;
		m_pHeaderList->Free(tempTop);
		return;
	}

	assert(false && "PLEASE DE_ALLOCATE MEMORY IN REVERSE ORDER WITH A STACK ALLOCATOR");
}